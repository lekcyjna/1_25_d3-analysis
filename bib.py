import numpy as np
import pandas as pd


def readFileWithFPKM():
    df=pd.read_csv(
        "../../X204SC22061884-Z01-F003/03.Result_X204SC22061884-Z01-F003_Homo_sapiens/Result_X204SC22061884-Z01-F003/3.Quant/1.Count/gene_fpkm.xls", 
        index_col=0, sep="\t")
    return df

def filterOutColumns(df : pd.DataFrame, columnNames):
    return df.drop(columns=columnNames)

def splitSamples(df : pd.DataFrame, dictOfNames : dict[str,list[str]]):
    out={}
    for setName in dictOfNames:
        out[setName]=df[dictOfNames[setName]]
    return out

def addStatistic(df:pd.DataFrame, setName : str):
    mean=df.mean(axis=1)
    std=df.std(axis=1)
    df.insert(len(df.columns), "mean_"+setName, mean)
    df.insert(len(df.columns), "std_"+setName, std)
    return df

def applyToAllCollections(splitedDf : dict[str, pd.DataFrame], f):
    out={}
    for k in splitedDf:
        out[k]=f(splitedDf[k],k)
    return out

def checkDifferentExpression(splitedDf : dict[str, pd.DataFrame], key1, key2):
    m1=splitedDf[key1]["mean_"+key1]
    m2=splitedDf[key2]["mean_"+key2]
    fpkmChange=np.log2(m1/m2)
    return fpkmChange

def checkDifferentExpression_StdTimes2(splitedDf : dict[str, pd.DataFrame], key1, key2):
    m1=splitedDf[key1]["mean_"+key1]
    m2=splitedDf[key2]["mean_"+key2]
    s1=splitedDf[key1]["std_"+key1]
    s2=splitedDf[key2]["std_"+key2]
    maxS=np.maximum(s1,s2)
    diff=m1-m2
    fpkmChange=np.log2(diff/maxS)
    return fpkmChange

def genColumnComparison(splitedDf : dict[str, pd.DataFrame]):
    out={}
    for k1 in splitedDf:
        for k2 in splitedDf:
            if k1<=k2:
                continue
            out["log2FpkmChange|"+k1+"|"+k2]=checkDifferentExpression(splitedDf, k1,k2)
            out["log2FpkmChange_std|"+k1+"|"+k2]=checkDifferentExpression(splitedDf, k1,k2)
    return out

def getGeneDataColumns(df : pd.DataFrame):
    return df.filter(regex="gene_|tf_family")
